<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Research Methods</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 360, Winget" />
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Research Methods
### PSYC 360, Winget

---




background-image: url('assets/img/img01.png')
background-size: contain

???

+ Today we're going to talk about something that's not the most exciting...but it's actually really important for us to consider this as we're going through the course
+ You're all coming at this with different backgrounds and experience in psychology
+ So I want to make sure we're all on the same page on how we understand methodology
+ You have to understand the methods we're using to get the research you're reading about
+ This will help you understand what the research has found
+ It will also help you figure out how you want to critique the research you're reading about
+ So we'll go through the basics of the scientific method as it applies to psychology
+ 
+ Now, psychology is a science
+ Some people think only biology and physics get that title...or make a distinction between hard and soft sciences
+ In reality, there isn't really this distinction
+ Any field that uses the scientific method is a science...by definition
+ Psychology approaches questions...even big huge questions like prejudice...what is it...how do you reduce it...we approach questions with a systematic method
+ We state the problem...define the purpose...do some research to find out about the topic 
+ This research to find out about the topic usually involves looking at past research...what already exists there
+ You rarely start designing a study without reading the existing research
+ In fact, you should not...because someone might have already done it...you shouldn't be reinventing the wheel
+ So you see what's out there...you come up with your predictions about the problem...do an experiment or some sort of study to test that question...analyze your data...and state the conclusion you make from those results
+ So compare what you found in your data to your hypotheses...and this often starts a cycle where you come up with new questions based on what you find
+ You'll often have to go through a much more extensive process if you want to publish this in a journal or if you want it to get into a textbook like the one that you're reading for this course
+ 

---
#  How to read a journal article*

.pull-left[
+ Types of articles
  + Research report
  + Review article
  + Theoretical article


+ Anatomy of a research report
  + Title and abstract
  + Introduction
  + Method
  + Results
  + **.red[Discussion]**
]

.pull-right[
.right[&lt;img src="assets/img/img02.jpg" width=42%&gt;]

**.red[Overwhelmed?]** &lt;br&gt;
**.red[&lt;--- Start here!]**
]

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[_*adapted from Jordan &amp; Zanna (1999)_]

???

+ There's a reading on Sakai for you that tells you how to read a journal article...it was assigned in the week 1 module
+ I don't assume that all of you have read one before...they're a very different taste than textbooks or what you might read in the popular press
+ So, I just want to go through some of the sections really quickly together
+ There are a few different types of articles that you might come across
+ There's a research report...most of the time, if there are supplemental readings in this course, they're going to be research reports
+ The book that you're reading combines different theories, but it draws on research reports...and to some extent review articles
+ Review articles tell you what exists in the scientific literature on a particular question...like gender and leadership for example...there might be a review paper talking about everything we know about gender and leadership
+ Within a research report...we're going to start there because the ones that you'll be reading as supplements for class will be research reports...they start out with a title and an abstract
+ The title is a super short take home message that you might tell your friends about
+ The abstract is about 150 to 200 words that tells you really concisely about the entire paper...it's a general gist of the paper
+ In the introduction, researchers will review the past research and tell you where they're perspective is coming from
+ Hopefully, the hypotheses make sense by the time you get to the end of the introduction
+ Method section....this is the most detailed part of the paper
+ This will tell you how they operationalized their constructs...so how did they take "prejudice" and define it concretely to participants in their study
+ This is what you're going to want to look at to see "do I buy what they're selling"...do I agree that what they said is prejudice is actually prejudice
+ The results section...this is going to have a lot of statistics in it...a lot of numbers
+ It's good to look at this...you may not have the tools to really understand that part quite yet...so don't worry if you don't
+ Instead, you can skip to the discussion section of the article....and in fact, I recommend a lot of times that you start with the discussion
+ Which sounds a bit weird, right....but if you start with the discussion, the researchers are actually telling you the take home message...what did they find...they're linking it back to the literature...and they're poking holes in their own research
+ So, when in doubt, start with the discussion section
+ Often times, in research in order to get published, you have to do more than one study
+ So we'll frequently find an article that has 2, 3, even 4 studies in it

---
#  How to read a journal article

.pull-left[
+ Reports with multiple studies
  + Same basic structure
  + Multiple method, results, and/or discussion sections


+ Reports as storytelling
]

.pull-right[.right[&lt;img src="assets/img/img03.png" width=45%&gt;]]

.center[&lt;img src="assets/img/img04.png"&gt;]

???

+ So with these reports that have multiple studies...it follows the same basic structure...it just repeats itself
+ So you'll just have an overall introduction...then you'll have multiple method sections, multiple results, and multiple discussion sections
+ All this is generally sandwiched between a big introduction section and a big discussion section
+ All of these research reports or journal articles are telling you a story
+ They're telling you a story about a problem...one way they tried to test an explanation for that problem...and what they found
+ So your job as a reader is to decide how much you believe their story...how much do you believe the way they're telling that story
+ They'll start with a big, broad idea...then show you the empirical support for that idea...followed by hypotheses...and so on
+ Their story will follow this sort of hourglass shape for their paper
+ So your job is to follow them through this story and figure out how much you buy it
+ The goal of science and psychology is to explain facts...what are the facts...why did something happen
+ Any fact by itself, in isolation, is just a piece of trivia
+ But when you combine a bunch of facts together...under a theory...that's where psychology becomes a science
+ So we're trying to combine facts within a theory and that's how we get to be a science

---
#  How to read a journal article

+ The rest of the story
  + Research reports are not objective; they are meant to persuade you
  + Therefore, we need to be **.blue[CRITICAL CONSUMERS]** of research...
  + ...but not so critical that you cannot gain important insights

&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/img05.png" width=30%&gt;]

???

+ Remember, these reports are not objective
+ None of us are objective when we set into it...we all go into it with our own biases...our own preferred interpretation of the outcomes
+ The researchers are trying to persuade you
+ So you should be a critical consumer of research...same thing with the media...be a critical consumer of media
+ Any media that you're listening to...reading...absorb it...but then think "hmmm, do I agree with this?"..."do I agree with their assumptions...with what they're trying to tell me"
+ These days you have to even ask "is this news even real?"...right...sometimes it is, but sometimes it's not
+ But especially if it's research published in an academic journal...or research that's published in your textbook...try not to be so critical that you can't gain important insights from it
+ Because once it's been published in peer-reviewed journal articles...once it makes it into a textbook...it's gone through a very rigorous process of review and evaluation
+ For example, to get a paper published...

---
background-image: url('assets/img/img06.jpg')
background-size: contain

???

+ This is sort of a little funny graphic of the peer-review process
+ You have to make it all the way from writing up your report
+ You might think that's good enough...but no, you have to make it through all of the peer-review process
+ The longest that it's taken me to publish from starting to have an idea...to actually getting the article published...was about 5 years
+ It was a large data set, so it took a little over 2 years to collect the data...we were interested in how stereotype content related to different moral foundations...and then it took another year or so to analyze everything and write up the report
+ The peer review process along took a little under 2 years...it was actually one month short of 2 years
+ So in this peer-review process...you're basically sending it out for anonymous review...you're having qualified peers basically tear your research apart...that's their job...to poke every hole possible in your research
+ And you have to go back and explain why it's still legit...why it's still valid...why it's worth publishing
+ Hopefully through that process, it gets to be a better paper...mine certainly did
+ But this is just to say that any paper you're reading has been through that gauntlet
+ So, we're always trying to do better as a science...there's always ways to improve the papers that have already been published...but what you're going to be reading has already been through that process
+ So bring a critical mindset...but not such a critical mindset that you can't absorb the information
+ Okay, so back to methods

---
#  Hypotheses and predictions

+ Hypothetical constructs
  + Abstract constructs stated in theories (e.g., prejudice)


+ Operational definitions
  + Concrete representations of constructs (e.g., score on prejudice questionnaire)


+ Predictions
  + Restate hypotheses in terms of operational definitions

.right[&lt;img src="assets/img/img07.png"&gt;]

???

+ Let's talk a bit about hypotheses and predictions
+ We always start out with hypothetical constructs
+ A hypothetical construct is something abstract...it's an abstract construct that's stated in your theory
+ For example, we're talking about prejudice...so that's a very abstract construct
+ You could also think about love...or emotion...or aggression
+ All of those things are very abstract ideas that you might state as part of your theory
+ So the construct is at a very big and broad level
+ You can't directly observe that construct
+ It's so big...it's so broad...that every single person in this class could come up with a different definition of prejudice
+ So, how do we study that scientifically?...well, we have to operationalize it...we break it down into something we call an operational definition
+ That operational definition is a more concrete representation of the construct
+ So if you've had research methods...or are taking it now...hopefully this is ringing a bell
+ If you haven't had it yet...this is your first really quick and dirty introduction to it
+ But the operational definition is a concrete representation of that big, broad construct
+ For example, with prejudice, we could say "I'm defining prejudice as a score on this particular prejudice questionnaire"
+ So whereas you cannot directly observe the hypothetical construct, you can absolutely can observe your operational definition...and it's very important that you do so, so that you can see what people are talking about when they present their research
+ This is how they define it...do you agree that what they define as prejudice is in fact prejudice
+ These things lead us to state our predictions
+ Our predictions are our hypotheses that are restated in terms of the operational definitions
+ You can state a prediction at a conceptual or construct level...they can be very broad...something like prejudice happens because of these things
+ Or you can get more concrete and specific with your operational definitions in your hypothesis
+ So with your prediction, you're restating your hypotheses in terms of that very precise operational definition
+ So you've got your idea...reviewed the literature...you have your hypotheses...created your operational definition...and made your predictions very concrete

---
##  Drawing conclusions from research

+ Support hypothesis? 
  + Statistical analysis of data
  + Qualitative analysis of narrative


+ Interpret data
  + Generally consistent with preferred theory?


+ Verify results
  + Direct replication
  + Conceptual replication


+ Generalizability 
  + Does this work with many people, different contexts and cultures?

.right[&lt;img src="assets/img/img08.png" width=30%&gt;]

???

+ How do we draw conclusions from the research that we conducted?
+ The first question you want to know is did you support your hypothesis?
+ Did you support it or contradict it...how do you get there?...you have to statistically analyze the data
+ There are tons of different ways of doing that...in the last few years, there have been more and more discussions about the right and wrong ways to do this...because people can get a little bit shifty when they're doing it
+ But again, that's part of the peer-review process...as a reviewer, you're going to make sure people are using the appropriate statistics
+ We now have this open repositories for data...where you are part of a scientific community and post your data so that other people can analyze it and verify the results
+ Most of the stuff you'll read about will be based on statistical analysis...more rarely in psychology, but we still do it sometimes, is more qualitative analysis of a narrative
+ So you might interview a person and analyze that qualitative data...you might have people be recorded and code what they tell you as part of a study
+ But you then have to interpret the data from your analysis
+ Here, you're looking to see if this is consistent with the theory...is it inconsistent
+ There's a bias towards publishing things that are consistent with a theory...something call publication bias...which is a bit problematic...we want to know what is inconsistent just as much as we want to know what is consistent
+ But because of this publication bias...most articles you'll read are consistent with the theory
+ Good researchers always verify their results...it's a cornerstone of good science
+ In the last few years...there has been a greater push for direct replications
+ Direct replication means you do the study again exactly the same way it was done the first time
+ So I collect a sample of 150 people...I give them a particular set of survey questions...I analyze the data...and then I do it again...exactly the same way
+ Another type of replication is called a conceptual replication...where you tweak something just a little bit
+ So you switch out an operational definition for example...instead of one measure of prejudice...you use a different measure of prejudice
+ Generalizability is something that psychology struggles with a little bit
+ With generalizability...what you're asking is...does what I found work with many people in different contexts and in different cultures
+ Does this generalize to more than just the people and situations I observed in my study...or is it limited to just these people or just these situations
+ Most psychological research is done in western cultures...it's done in the US or Western Europe
+ And for those reasons...we know a lot about people from those countries and those cultures
+ We know significantly less about people from different countries and cultures because they haven't been as involved in the research process
+ So we need to do a better job of generalizability...and that's one way you can apply your critical mindset to the research you're reading by saying "huh...I wonder if this would work the same way in this different context or this different culture"
+ And if you have ideas specifically about how and why it should differ...maybe you have yourself an idea for a study

---
#  Correlational method

+ Systematically measure two variables and the relationship between them
  + From knowing X, can we predict Y?
  + Watching violent TV and aggression
  + **.blue[Ranges from -1 to +1]**

&lt;br&gt;
.center[&lt;img src="assets/img/img09.png" width=100%&gt;]

???

+ Okay, let's talk a little bit about correlational vs experimental methods
+ With the correlational method...that's where you're systematically measuring two variables and the relationship between them...at least 2 variables...often, we'll measure a bunch of variables at the same time and look for correlations between them...but you need at least 2
+ You're going to measure those variables and systematically see what the relationship is between them
+ So this is not a stats class but we do need to understand how to interpret original research...so we go a little bit into what a correlation means...what it is
+ With correlational methods...we're interested in seeing if we know X can we predict Y
+ For example, you might ask from knowing the amount of television watched...could you predict someone's aggression
+ A correlation ranges from -1 to +1...it will never be less than -1 or greater than +1
+ +1 means as one variable increases, the other also increases...an example of that is on the left here
+ 0 means theres no association...no relationship between them...like in the middle graph..it sort of just looks like a random scatter of data points
+ And -1 means as one variable increases, the other decreases...like in the right graph
+ You've heard this before I'm sure...but we have to say it again...and keep repeating to yourself that...

---
#  Correlation ≠ Causation!

+ Even if A and B are perfectly correlated, we cannot determine their causal relationship


+ Three possibilities:
  + A causes B (more prejudice, less contact)
  + B causes A (more contact, less prejudice)
  + A third variable, C, is causing both A and B

&lt;br&gt;
.center[&lt;img src="assets/img/img10.png" width=80%&gt;]

???

+ Correlation does not equal causation
+ Just because you know two variables are related to each other does not mean one causes the other
+ You can't tell that from correlational studies
+ You cannot tell that one variable causes another unless you experimentally manipulate something
+ Even if A and B are perfectly correlated...we cannot determine their causal relationship
+ Let's take an example in the context of this class...intergroup contact and prejudice
+ Even if the amount of intergroup contact and prejudice had a -1 correlation...even if they were perfectly correlated...we still could not determine a causal relationship...we can't say for sure one causes the other
+ Why?...well 3 things could be happening
+ A could be causing B...maybe the more prejudice someone has, the less contact they seek out with people from other groups...that could be explaining the negative correlation
+ So A could cause B...if you're a prejudiced person, you don't have contact with that other group...that's one interpretation
+ But, it could be that B causes A...the more contact you have with people...the less prejudice people have
+ If you're just measuring these 2 variables at the same time...you don't know which is true
+ And just to mess with you a little more...there's this third possibility
+ And that is that you could have a third variable...variable C...something you haven't even thought about...something you haven't measured...and that third variable is actually causing both A and B
+ So in the case of intergroup contact and prejudice...maybe friendship is linked...maybe friendship is this C variable
+ So maybe friendship is causeing people to have both less prejudice and more contact...and unless you measure that, you wouldn't know
+ So you might have part of the picutre with a correlational study, but you can't tell exactly what's going on
+ Now, you might ask...why do we care so much about knowing the cause...well knowing the cause allows us to prevent or change things

---
#  Correlational method

+ Survey
  + A sample of people is asked questions about their attitudes or behavior
  + Validity depends on representativeness of the sample

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/img11.png" width=50%&gt;]

???

+ So with the correlational method, we usually collect information with surveys...not all surveys use correlational methods, but some do
+ Pretty much all of you have completed some sort of survey
+ With a survey, you get a sample of people and ask them about their attitudes or behavior
+ If you want to make a little money...you can become a Prolific worker
+ Some of you may have hear of Amazon's MTurk...Prolific is very similar except that you're actually paid a fair wage
+ Prolific requires that workers are paid at least $6.50/hr...which means you can make at least a dollar or 2 for taking a 15 minute survey
+ So with a correlational survey, we get a sample of people and ask them about their attitudes or behavior
+ How valid that is...how valid the conclusions we can make are...depends on the representativeness of the sample
+ So if I sampled everyone in this class...the the ability to generalize to everyone in the US would be pretty low...because you're all university students
+ My ability to generalize to university students might be descent-ish...but this is also private Jesuit university...so that could make the generalizability a bit questionable
+ My ability to generalize to Loyola students would be a bit stronger...but my best chance would probably be to say that I could represent psychology students at Loyola...but even then, there's some self-section issues since you all chose to sign up for this class
+ So the validity of the survey depends on how representative the sample is

---
#  The experimental method

+ Provides information about causality
  + Is variable X a cause of variable Y?


+ Experimenter alters one variable to see what effect it has on another


+ **.blue[Independent]** versus **.blue[Dependent]** variables

???

+ So, if we need to know what causes something, we often want to use the experimental method
+ Using the experimental method let's us say is variable X a cause of variable Y
+ As an experimenter, you alter one (sometimes more than 1) variable to see what effect it has on another variable
+ Does changing one variable change another variable
+ This leads us to the difference between independent variables and dependent variables
+ IVs are the ones that you as a researcher are going to control or manipulate
+ So that's the variable you change
+ We talked about intergroup contact....you might assign some people to have high intergroup contact and others to have low intergroup contact
+ As the experimenter, I am controlling the manipulation of that variable
+ The dependent variable *depends* on the levels of the independent variable
+ So if you think contact causes a change in prejudice...then your dependent variable has to be prejudice
+ You're looking to see if prejudice depends on the levels of contact that a person experienced

---
#  Experiments: Internal validity

+ Internal validity
  + Controlling all variables except the independent variable


+ Random assignment to condition
  + .red[Most important part of the experimental method]


+ Control groups
  + Randomly assign some kids to have intergroup contact, control group has only same-group contact

???

+ Another reason we like experiments and tend to have a bias towards these experiments is because it gives us something called internal validity
+ You get to internal validity by controlling all variables except for the IV
+ You want to increase the internal validity because the internal validity let's you say one thing caused another thing
+ That more intergroup contact caused less prejudice for example
+ So the stronger the internal validity, the stronger your ability to conclude causation
+ And you get good internal validity by controlling all the variables...all the noise...everything except that one IV that you want to manipulate
+ One thing that can help you do that is randomly assigning people to conditions
+ It's impossible to overstate the importance of random assignment in the experimental method
+ Random assignment means that you don't let people chose the condition they're going to be in
+ You use a random number generator or some other random method of getting people into your experimental conditions
+ So let's continue with our intergroup contact example
+ Let's say you wanted to have 100 people participate in a study...and you want to manipulate intergroup contact and you want have equal size groups
+ So you want to have 50 people in a low contact condition and 50 people in a high contact condition
+ So of those 100 people, 50 people have intergroup contact and 50 have same group contact...and at the end of it you measure their prejudice
+ But you feel kinda of bad for the people in the high contact condition because maybe they feel nervous about...and so you also give them some candy while they're in there...you're like here have some skittles...life will be good...and then you measure prejudice
+ If you did that...you had 50 in low contact...50 in high contact...but you gave the people in the high contact condition some candy...even if you saw a the predicted effect...that high contact reduced prejudice...could you say it was due to intergroup contact?
+ No...because you didn't tightly control everything else in the study
+ There was this other variable that systematically varied with the contact manipulation...candy
+ So you have to hold all of the conditions as constant
+ Now let's say that you got rid of the candy...you realized that was a rookie mistake...no candy...so you said "what I'm going to do is let people choose which condition they want to be in"
+ Let's say after that you still had 50 high contact and 50 low contact...and you still found a reduction in prejudice..could you say this reduction was due to high intergroup contact?
+ No...there's now this problem of self-section...it could be that non-prejudiced people are self-selecting into the high contact condition
+ So this is why random assignment is so important
+ Another thing you need to do is to try and have a control group in the study
+ A control group is a group that does everything else except it does get the manipulation
+ So maybe you have some sort of control group that just did the same dependent measures but didn't have any contact
+ If you had that, you could answer the question of compared to what
+ So if you randomly assigned people, and manipulated high contact...low contact..or no contact...if you still found high contact reduced prejudice...then you could say that randomly assigning them to have high intergroup contact reduced prejudice relative to those who had low contact and those who had no contact
+ It makes it more informative and increases your ability to make conclusions about this

---
#  Multiple measures

+ Self report
  + Attitudes, beliefs, behavior


+ Unobtrusive measures
  + Behavior, judgments


+ Physiological measures


+ Implicit measures


+ If multiple measures point in the same direction, we can be more confident in effect

???

+ We also want to make sure we use multiple different measures
+ A very common method is to use self report...where we just ask participants to rate themselves on certain measures
+ But this can also be a bit problematic
+ Whenever people are telling you their attitudes, beliefs, or behaviors...they might lie
+ They might be especially likely to like if you're asking them about prejudice and they're worried about looking like a biased person
+ In our culture, more and more these days, we don't want to look like we're biased
+ So we want to get around this and avoid having people underreport their bias
+ One way of doing this is through the use of unobtrusive measures
+ Just observing peoples behavior is one way of doing this...basically naturalistic observation studies
+ Sometimes we'll do that in studies...we'll say pull your chair up and have a conversation with this person...and measure the distance between the chairs as an operational definition of avoidance
+ Or in intergroup studies, we might record a conversation and code for the amount of eye contact they have with each other or the amount of smiling they engage in
+ Their behavior might give me a better read on their prejudice than a self-report item
+ Or you could have people make judgments like "well not you personally, but what do you think people in general would think of this scenario"...and people might be more willing to let their prejudice come through
+ We can also use physiological measures...your text will cover some of these...so that might be things like hooking people up to track their blood pressure or heart rate...we sometimes have people spit in tube to measure their cortisol levels...which tells us how stressed they are
+ These measures can often tell us something that people won't or sometimes can't report out loud
+ We'll learn about implicit measures...these are measures where we get your split second reactions to something
+ You'll learn about the implicit association test...where it's hard to control your responses to this test because it measures the milliseconds it takes you to associate social categories to certain evaluations
+ Implicit measures are often helpful when you're trying to learn about people's prejudice that they want to hide
+ And importantly, we want to have multiple measures of the same construct that point in the same direction
+ If a lot of measures...self-report, unobtrustive, implicit...are all telling us the same thing...we can be pretty confident about the effect
+ Alright...that about wraps up our methods lecture
+ Again...this isn't a stats or methods course...but understanding some of these things at a basic level will help use understand and critique the research we'll be engaging in this semester
+ So, I'll catch you in the next video were we start looking at some of this research as it relates to social categorization and stereotypes
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
