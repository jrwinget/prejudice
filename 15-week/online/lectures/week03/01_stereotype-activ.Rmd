---
title: "Stereotype Activation"
author: "PSYC 360, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#FED650",
  text_color = "#FFFFFF",
  background_color = "#3A3839",
  title_slide_text_color = "#3A3839",
  text_slide_number_color = "#3A3839",
  colors = c(
  red = "#FF0000",
  blue = "#00BFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

##  Stereotypes: Two contrasting views

.center[<img src="assets/img/img01.png" width=100%>]

???

+ Last week we talked about the social categorization process...we discussed the factors that lead to stereotyping...the problems of stereotypes...and how stereotypes are maintained
+ In this video we're going to dive into the nature of stereotypes a bit more
+ First, we'll cover the activation of stereotypes...and in the next video we'll talk more about the application of stereotypes
+ When we talk about how stereotypes factor into prejudice and discrimination, there are actually a few factors at work
+ First is the content of the stereotype...and as we saw in last weeks lecture and readings...stereotype content is widespread throughout society...and both prejudiced and non-prejudiced people know the content of most stereotypes
+ However, before a stereotype can have an effect...it has to be activated...and eventually this activated stereotype must be applied to the person
+ 
+ Now, there are generally two perspectives about this categorization and activation process
+ On the one hand, Allport put forth the notion that stereotypes are automatically activated and applied when we encounter a member of the stereotyped group
+ This is that cognitive account that we've discussed before...it's automatic...and due to the way our minds work...it's inevitable
+ And many other theorists have assumed that the ACTIVATION of a stereotype is an AUTOMATIC and INEVITABLE CONSEQUENCE of encountering the object of that stereotype
+ However, others make a different argument
+ The fact that people may employ stereotypes does not mean that they must ALWAYS do so
+ In 1989, Fiske argued that even though this categorization process is natural...and perhaps even a dominant strategy of trying to understand others...it's not the only option available
+ This perspective suggests that people can probably help it when they stereotype and prejudge
+ There are times when people put more effort into individuating a person from a certain social group and don't rely on the stereotype to guide their judgment of the person
+ So we'll consider this contrasting views and interpret them in light of what the research has to say on stereotype activation and application

---
#  Definitions

+ Activation
  + A stereotype becomes accessible in one's mind


+ Application
  + One uses a stereotype to judge a member of a stereotyped group

???

+ Now, it's helpful to begin with some definitions about what stereotype activation and application are
+ Stereotype activation is the extent to which a stereotype is accessible in a person's mind
+ Because a stereotype only applies to a given group...it's not useful except in relation to that particular group
+ So stereotypes are usually dormant until it's activated
+ This activation is usually brought on by something like an encounter with a member of a stereotyped group for example
+ But, stereotypes don't always end up being activated
+ In order for a stereotype to be activated...a person has to categorize someone a member of a stereotyped group....through the use of something like physical characteristics...like skin tone, facial features, or behaviors
+ Only after the stereotype has been activated can it be applied to the person
+ So that's stereotype ACTIVATION
+ Stereotype APPLICATION...on the other hand...is the extent to which a person uses a stereotype to judge a member of the stereotyped group
+ This whole process...from categorization...to activation...to application...is fast and automatic...rarely do people realize it's even happening
+ But just because this process is automatic does not mean that stereotype application is inevitable
+ Because group membership is not always clear-cut...a lot of factors can influence how a person categorizes another
+ Along similar lines...stereotype content does not always lead to activation...and activation of a stereotype does not always lead to its application
+ So let's break this down a bit

---
#  Which category is activated first?

+ Prototypicality
  + The person fits the perceiver’s idea of the essential features of that category
  + Skin color, facial features, hair length
  + Categorize high prototypical faster

.pull-left[.center[<img src="assets/img/img02.png" height=350>]]

.pull-right[.center[<img src="assets/img/img03.png" height=350>]]

???

+ The first step in the stereotype application process is categorization
+ Before a stereotype can be activated...the perceiver must categorize a person as a member of a stereotyped group
+ As we discussed last week...we tend to classify others into 3 basic social categories of gender, race, and age
+ But a single person falls into all three of these categories, right?...you could be a middle-aged, Black man...or a  young, White woman for example
+ Because a person can be placed into more than one category...several processes operate to determine which category gets the most attention...and therefore, which stereotype is available for activation
+ One factor that affects the speed and ease of categorization is prototypicality
+ A person is prototypical of a social category to the extent that they fit the perceiver's idea of the essential features that are characteristic of that category
+ Prototypicality is generally defined as the physical features associated with a category...so things like skin tone, facial features, hair length/texture
+ And the more prototypical a person is of a certain category...the more of these characteristics they possess...the more quickly and easily the person is categorized
+ For example, in one study, researchers used pictures of Black people who varied in skin tone from dark to light...much like the ones you see here
+ They found that the more prototypical the picture was to the Black social category...so the picture on the left here...this prototypicality facilitated racial categorization for BOTH White and Black participants

---
#  Which category is activated first?

+ Situation
  + When a person differs in some way from others in the same situation, that difference receives attention

<br>
<br>
<br>
.center[<img src="assets/img/img04.jpg" width=100%>]

???

+ Now even though categorization is usually automatic...situational factors can intervene in the categorization process to emphasize one category over another
+ The context in which a perceiver encounters another person can have a significant influence on categorization
+ For example, when a person in a group differs in some way from the other group members...onlookers pay the most attention to the category that the person differs from the others
+ Take this picture here....this is a picture of the computer science department from UC Davis
+ There is only one woman in this picture...in the bottom right corner
+ So when thinking about this person...people are likely to focus on her gender because she is in a context with all men
+ Even though she is also Asian...that category is less likely to be used because she is one of several Asians in this context
+ A person's behavior can also influence a perceiver's use of one category over another
+ In one study, researchers found that when people thought of an Asian woman...they were more likely to consider her gender when they saw her putting on makeup...but when they saw her eating with chopsticks...they were more likely to consider her race
+ So in these kinds of situations...perceiver's pay attention to and use the basic social category that the situation draws their attention to

---
#  Which category is activated first?

+ Prejudice
  + Racially prejudiced people pay more attention to race than other categories


+ Ingroup over-exclusion
  + Prejudiced people want to avoid treating outgroup members as if they were part of the ingroup

<br>
.center[<img src="assets/img/img05.png" width=45%>]

???

+ A third factor that can influence which social category is activated is prejudice itself
+ Racially prejudiced people tend to pay more attention to race than other characteristics...such as gender...when they first see a person
+ Prejudiced people are also biased in the categorizations they make
+ Specifically, they engage in something known as ingroup over-exclusion
+ Ingroup over-exclusion is where prejudiced people want to avoid treating outgroup members as though they were part of the ingroup
+ So they end up miscategorizing ingroup members as members of the outgroup
+ Even though they end up excluding ingroup members as a result...in their view, it is safer for them to do this than it is from them to misclassify outgroup members as ingroup members
+ It prevents ingroup privileges being granted to the quote/unquote wrong people
+ The exclusion of outgroup members can also be accomplished by accurately classifying people as members of the ingroup or outgroup
+ But...as we saw in our demonstrations last week...accurate classification can often be difficult...even when the person is in fact an ingroup member
+ This becomes even more difficult when the perceiver is confronted with a person who has ambiguous racial characteristics
+ In these cases, prejudiced people might be especially motivated to accurately classify people as Black or White, for example
+ And this is exactly what researchers found when they studied this

---

.center[
<img src="assets/img/img06.png" height=200>
<img src="assets/img/img07.png" height=200>
<img src="assets/img/img08.png" height=200>
<img src="assets/img/img09.png" height=200><br>
<img src="assets/img/img10.png" height=200>
<img src="assets/img/img11.png" height=200>
<img src="assets/img/img12.png" height=200><br>
<img src="assets/img/img13.png" height=200>
<img src="assets/img/img14.png" height=200>
<img src="assets/img/img15.png" height=200>
<img src="assets/img/img16.png" height=200>
]

???

+ (page 126)
+ In the study, researchers had participants view pictures...much like the ones you see here
+ They were presented the pictures one at a time and in random order
+ Once a picture was shown...they were asked to identify the race of each person as quickly and accurately as possible
+ The manipulation here was how racially ambiguous the pictures appeared to be
+ Unambiguous pictures are those like the top and bottom rows...and more ambiguous ones where those in the middle
+ The results showed that racially PREJUDICED PEOPLE TOOK LONGER to categorize RACIALLY AMBIGUOUS FACES as Black or White than did less prejudiced people
+ Presumably they took longer because they spent more time trying to be accurate in their classifications
+ Prejudice and non-prejudiced people DID NOT DIFFER IN HOW LONG IT TOOK THEM TO CATEGORIZE UNAMBIGUOUS (or these prototypical) faces

---
#  Stereotype activation

+ Dormant stereotype becomes active, ready for use, capable of influencing thoughts and behavior toward stereotyped group member
  + Automatic processing
  + Motivated processing

<br>
.center[<img src="assets/img/img17.png" width=60%>]

???

+ Now, once a person has been categorized as a member of a stereotyped group...the stage is set for stereotype activation
+ In stereotype activation...a dormant stereotype becomes active...ready to use...and capable of influencing a person's thoughts and behavior toward a member of the stereotyped group
+ So after a person has been categorized...two types of stereotype activation processes begin that operate simultaneously and can affect one another
+ The first type...automatic processing...is triggered by simply observing stimuli associated with the stereotyped group
+ For example...seeing a rainbow triangle might trigger automatic processing for the LGBT category
+ The second type...motivated processing...is rooted in people's goals and needs...and this second type is activated when the stereotypes can help fulfill those goals and needs
+ These motives can come from either individual differences...like personality...or from more situtational factors
+ Activation for motivated stereotypes only occurs if some aspect of the stereotype is relevant to the situations the person is in
+ For example, in selecting someone for a job...stereotypes that include work-related traits...like lazy or hard-working...would be activated...but stereotypes that do not include work-related traits would not be activated
+ So let's break down each of these processes respectively to see how they work and influence one another

---
#  Automatic activation

+ No effort or conscious thought


+ Subliminal priming of Black or White face


+ Stereotypical completion of word stems when primed with Black face


.pull-left[.center[.large-text[
Hos____


Wel____


Ste____


Stu____
]]]

.pull-right[.center[<img src="assets/img/img16.png" height=350>]]

???

+ As we've talked about...categorization paves the way for stereotype activation
+ Like categorization...stereotype activation is automatic...it occurs without effort or conscious thought once a person has been categorized as a member of a stereotyped group
+ The automatic nature of stereotype activation is evidenced by research that shows even subliminal cues...cues that are presented too quickly to be consciously processed...can activate stereotypes
+ For example...researchers had participants watch a computer screen where either a Black or White face was shown for less than a tenth of a second...too fast for them to consciously process the image
+ After this, participants then had to complete word stems...like the ones you see on the screen here
+ Participants who saw Black faces were more likely to complete the stems with words consistent with the Black stereotype...they used words like hostile, welfare, steal, and stupid
+ On the other hand, participants who saw White faces were more likely to complete the stems with words with nonstereotypic terms....they used words like hospital, welcome, step, and student
+ When automatic processing does occur...researchers believe it stems from people's cultural environments that establish links between a category...such as Black...and stereotypes associated with that category...such as lazy, musical, athletic, and hostile
+ Because these category-stereotype links are so strong and pervasive in the culture...people learn the links so thoroughly that the stereotype becomes a conditioned, automatic mental response to the category
+ There is a lot of research that supports the idea that when a category label is presented...people are unable to prevent make the association between the category label and the information they've stored about it
+ The strength of these associations is often measured by how quickly people respond to stereotypic words after a category is primed
+ For example...when White participants are presented with the racial category Black...they often respond more quickly to traits stereotypically associated with Blacks and to negative traits in general
+ However, when a White prime is presented...responses are fastest for traits stereotypically associated with Whites and for positive traits in general
+ This suggests that Whites tend to see a stronger connection between White and positive...and Black and negative than the reverse
+ This demonstrates that people not only access a category faster when they are primed...they also access the evaluations they associate with that category

---
##  Automatic activation <br> (Gilbert & Hixon, 1991)

+ Conventional wisdom & research say...
  + Stereotypes = shortcuts
  + People should be particularly prone to use stereotypes when they are short on energy (Bodenhausen, 1990)


+ But, one might need resources to activate the stereotype
  + No resources, no stereotype activation
  + White or Asian person holding card: ri_e

.right[
<img src="assets/img/img18.jpg" height=215><br>
Gilbert
]

???

+ Now, until the 1990s or so...most researchers accepted the inevitability of category activation leading to stereotyping
+ But, Gilbert and Hixon thought...MAYBE stereotypes must be ACTIVATED before they can be APPLIED to perception and judgment
+ If that’s true, contrary to the conventional wisdom...cognitively busy perceivers may occasionally be LESS likely than NOT BUSY perceivers to CONSTRUE OTHERS IN STEREOTYPIC TERMS
+ This is because stereotypes are stored in long-term memory...remember, they lie dormant until they are activated...and only once they're activated can they be brought into working memory where they can be of use
+ So the idea Gilbert and Hixon had was if working memory is in use...if people are cognitively busy...stereotype activation might be able to be disrupted because little space would be left in working memory for stereotypic information
+ So they decided to tested whether COGNITIVE BUSYNESS CAN INHIBIT THE ACTIVATION OF STEREOTYPES in a now classic study...(exp 1)
+ They had White participants watch a videotape where either a White or Asian RA showed the participants a card with one missing letter
+ The participants had 15 second to generate as many words as they could based on this word fragment
+ Five of the word fragments could be completed as either as words that stereotypically describe Asians or as nonstereotypic words
+ For example...RI_E could be completed as either the stereotypic rice...or the non-stereotypic ripe
+ The researchers hypothesized that if cognitive resources available, seeing the Asian RA would activate the Asian stereotype and lead to more stereotypic word completions
+ However, if participants had no cognitive resources available, there would be no activation of stereotype...which should lead to more non-stereotypic word completions
+ To manipulate cognitive business...they had the participants rehearse an 8-digit number while watching the videotape...rehearsing this number should use up their working memory
+ They found that of the participants who saw the Asian RA, those who were cognitively busy completed fewer stereotypic words than those who were not cognitively busy
+ Participants who saw the White RA, made the same number of stereotypic word completion in both the busy and non-busy conditions
+ So, seeing the Asian RA activated the Asian stereotype for people who were not cognitively busy...but not for those who were cognitively busy

---
#  Motivated activation

+ Stereotypes are activated or inhibited to achieve goals
  1. Comprehension
  1. Self-enhancement
  1. Social adjustment
  1. Motivation to control prejudice


<br>
<br>
<br>
.right[<img src="assets/img/img19.png" width=50%>]

???

+ So we've talked about automatic processing...let's shift gears a bit and talk about this second type of stereotype activation...motivated activation
+ Research shows that people's motivations can facilitate or inhibit stereotype activation
+ In other words, people have goals they want to achieve in various social settings...and when stereotype application can help satisfy these goals...stereotypes are activated for that purpose
+ However, when stereotype application can disrupt a person's goals...stereotype activation is inhibited to prevent such application
+ Now, there are clearly a lot of different motivations people could have regarding these goals that could affect stereotype activation
+ We'll focus on the 4 broad categories that the textbook covers...comprehension goals...self-enhancement goals...social adjustment goals...and motivation to control prejudiced responses
+ These goals can come from a variety of sources...such as individual differences, situational factors, and in some cases, both
+ But, it's important to point out here that more than one goal can operate at any given time
+ So, two goals...for example...could reinforce each other if both motivate stereotype activation or inhibition
+ On the other hand...one goal could offset another goal if one motivates stereotype activation and the other motivates stereotype inhibition
+ Clearly...things can get pretty complex pretty quick here
+ So, let's start with the basics and consider each of these goals individually first

---
#  Motivated activation

+ Comprehension
  + Need to form accurate impression of others and understand why events happen


+ Asian experimenter asked White participants questions unrelated to race
  + **.blue[Goal:]** Form impression of experimenter *personality* and career choice OR focus on *topics of interview*
  + Stronger stereotype activation for participants trying to understand personality

???

+ Comprehension is about our need to form accurate impressions of others...and our need to understand why events happen
+ One of our fundamental needs as human beings is the ability to predict and control our world
+ That's why we have all of these super computers..and smart technology...it allows us to make fast decisions to predict and control our world to some extent
+ So one goal we might have is comprehension...we might need to form an accurate impression of other people and understand why events happen
+ And we can use stereotypes...we can activate them or inhibit them strategically...to form an accurate impression of other people
+ In one experiment...researchers found that people did activate a stereotype to try to form an impression of another person
+ So in this study, they had an Asian experimenter ask White participants questions....and none of those questions had to do with race...so these were totally race-neutral questions
+ What they varied in was the participants' goals in the study
+ They either had a goal to form an impression of experimenter's PERSONALITY AND CAREER CHOICE...so if you were a participant in this study you're told that as this experimenter is talking to you, try to form an impression of her personality and her career choice
+ OR participants in a different condition were told...just focus on the topics of the interview
+ Remember...nothing had to do with race, but people did have different goals
+ And the question is whether they used the experimenter's race to help them form an accurate impression
+ What they found is that there was stronger stereotype activation when participants were trying to understand her personality
+ So in this case, they were thinking..."this stereotype can help me form a more accurate impression of her personality and career choice"
+ So they used it strategically to fill in the blanks and tell them more about her personality
+ They used that stereotype as information
+ They didn't get as much stereotype activation when they were just asked to focus on the topics of the interview...so people can set it aside, but they'll use it when they think it's going to be meaningful
+ And again, this goes back to if we're pressed for time or if we don't have a lot of cognitive resources...a lot of times we might, if we do have to make an impression of someone and have a comprehension goal, we might rely on stereotypes to make that happen

---
#  Motivated activation

+ Self-enhancement
  + Need to see self in positive light


+ Participants receive positive or negative feedback from a Black physician (Sinclair & Kunda, 1999)
  + **.blue[Negative feedback:]** Activate Black stereotype, inhibit physician stereotype
  + **.blue[Positive feedback:]** Inhibit Black stereotype, activate physician stereotype

.right[<img src="assets/img/img20.png" height=275>]

???

+ So, the second goal that research looks at in terms of motivation is something called self-enhancement
+ Self-enhancement is the need to see ourselves in a positive light
+ Everyone wants to think they are a good person...that they're a moral person..that they're kind and caring
+ And self-enhancement is what we do to try to make sure that we see ourselves in a positive light
+ This involves minimizing negative feedback...maximizing positive feedback...and going through various steps to make that happen
+ So in another study...researchers looked at the role of self-enhancement in the use of stereotypes
+ In this case, they brought participants into the lab and they randomly assigned people to receive positive or negative feedback from a Black physician
+ So in every case, they got feedback from a Black physician...but it was either good news or bad news
+ So when you're talking about a Black physician...you can focus on a couple of different parts of that person
+ You can either focus on that person's race...or you can focus on that person's profession
+ So you could choose to attribute the feedback that you get to the person's race...or to the fact that they're a medical doctor
+ And researchers wanted to see if people would strategically manipulate what feedback they paid attention to in order to enhance themselves
+ So whenever people got negative feedback from the physician...they found that the Black stereotype was activated...and the physician stereotype was inhibited
+ So why might this be the case?....If you're getting negative feedback...why would you want to activate the Black stereotype and inhibit the physician stereotype?
  + Well...doing this let's you say "that person doesn't know what they're talking about"...people focus on the race to discredit the person...so that they don't have to take the feedback seriously
+ The researchers also found in this study that when people got positive feedback...that inhibited the Black stereotype and activated the physician stereotype
+ So when it's good news...we want to think that it's about us and take it seriously...so they activated the physician stereotype to be able to take more credit for that praise...and inhibit the stereotype of the person's race

---
#  Motivated activation

+ Social adjustment
  + We want to fit in, follow norms
  + Avoid offending others
  
  
+ White participants show **.blue[less]** stereotype activation when they have a **.blue[Black experimenter]** than a White experimenter 

<br>
.center[<img src="assets/img/img21.png" width=50%>]

???

+ The third goal they've looked at with research is social adjustment
+ Social adjustment basically just means wanting to fit in and follow the norms
+ Most people want to be accepted by other people...and there are certain ways we can behave to do that
+ So if you want to fit in, you'll often follow what the norms..or the rules...are for a situation
+ And in society, we sometimes have the rule that you should avoid offending other people
+ So try not to offend other people...try to fit in...try to follow norms
+ For example, you might talk about a topic like dating one way with your friends...but a very different way with your parents...because you want to shift and follow the norms for that situation
+ A bunch of different kinds of studies have found that White participants show LESS stereotype activation when they have a BLACK EXPERIMENTER than when they have a White experimenter 
+ So simply having a Black experimenter in the room, gets White participants to show less stereotype activation than when they have a White experimenter
+ Some of this research is referred to as "social tuning"...we won't talk about it a ton this semester...but just having a person from a racial minority group in the room...if people are motivated to avoid offending others...it's dampening down the activation of the stereotype
+ So that's kind of good news
+ And they've even measured this with millisecond reaction times...like the Implicit Association Test that we'll look at more here in a couple of weeks
+ So social adjustment is a third goal...another goal people could have that's related to this is the motivation to control prejudice

---
#  Motivated activation

+ Motivation to control prejudice


+ Prejudice is considered a negative trait, so avoid looking prejudiced
  1. Against personal values
  1. Don’t want others to think we are biased
  1. Chronic egalitarian goals, less likely to activate stereotype

<br>
<br>
.center[<img src="assets/img/img22.png" width=40%>]

???

+ So some people are motivated to control prejudice
+ Prejudice...in general...is considered a negative trait...so people want to avoid looking prejudiced
+ Even if they know they're prejudiced...they'll say they're not prejudiced
+ Because we're supposed to be egalitarian...everyone's supposed to be equal
+ They'll say things like..."I want to treat everyone equally...I'm not biased...I'm good because I'm not racist"
+ So prejudiced is considered a negative trait...so MOST people want to do what they can to avoid looking prejudiced
+ They can do this for a number of different reasons
+ One is that looking prejudiced might be against their personal values
+ For some people...being egalitarian is deeply ingrained...it's part of who they are
+ So they say...I don't want to appear prejudiced because doing that would my goal...I would feel like a bad person if I was biased
+ So they might be motivated to control prejudice because being prejudiced goes against their personal values
+ Other people...simply don't want others to think they're biased
+ So this second one here is more of an external motivation...the first one is more of an internal motivation that's coming from the person's own values
+ But with this second one...you just don't want other people to think you're biased...even if you know you secretly are
+ A third way this could happen is people who have chronic egalitarian goals...I like to refer to this people as the unicorns...because they very rarely exist
+ But there are some people who just have these egalitarian goals...egalitarian just means that you want to treat everyone equally...some people will have these chronic egalitarian goals just always accessible...those people they've found are less likely to activate stereotypes
+ Chronic egalitarians are about 10% of the college population
+ And the way they've measured this is by giving people the actual measure for it...but in addition they also measure their explicit and implicit prejudice to determine true chronic egalitarians
+ So at this point I want to wrap up the stereotype activation lecture and have you take the internal and external motivation to control prejudice scale located in this week's module
+ You won't turn this in...it's just for you...so do what you will with that information
+ I recommend answering the questions honestly...because no one will see your answers
+ Only look at the document labeled scale...don't look at the document labeled scoring until you've filled out your answers...no cheating!
+ Make sure to carefully read the instructions before you fill out your answers
+ So that's a wrap for stereotype activation
+ In the next video, we'll discuss what happens during the application process after a stereotype has been activated
